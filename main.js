const express = require("express");
const app = express();
const PORT = 3000;

// Baseurl: http://localhost:3000
// Endpoint: http://localhost:3000/
app.get('/', (req, res) => {
  res.send("Hello world");
});

/**
 * This is arrow function to add two number together.
 * @param {Number} a is the first number
 * @param {Number} b is the second number
 * @return {Number} This is the sum of the a and b params.
 */
const add = (a, b) => {
    const sum = a + b;
    return sum;
};

app.use(express.json());  // middleware
// Endpoint: http://localhost:300/add
app.post('/add', (req, res) => {
    const a = req.body.a;
    const b = req.body.b;
    const sum = add(a,b);
    res.send(sum.toString());
});

// Listening fuction - starts server

app.listen(PORT, () => console.log(
    `Server listening at http://localhost:${PORT}`
));

console.log("Express app starting...");